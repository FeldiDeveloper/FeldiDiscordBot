package commands

import (
  "github.com/bwmarrin/discordgo"
)

type DiscordCommand interface {

    GetAliases() []string;
    GetName() string;

    GetCategory() []string;

    Execute(alias string, args []string, message *discordgo.Message, bot *discordgo.Session);
}
