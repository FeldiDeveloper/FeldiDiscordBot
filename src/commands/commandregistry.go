package commands

import (
  logging "github.com/op/go-logging";
  "strings"
  "fmt"
)

var log = logging.MustGetLogger("DiscordCommands")
var commandList []DiscordCommand
var initialized = false;

func InitCommands() {
  commandList = make([]DiscordCommand, 0, 16)
  initialized = true;
}

func RegisterCommand(cmd DiscordCommand){
  if !initialized {
    log.Error(fmt.Sprintf("Command list not initialized! Failed to register %s", cmd.GetName()))
    return
  }
  for _,cmd1 := range commandList {
    if cmd1.GetName() == cmd.GetName() {
      log.Error(fmt.Sprintf("Command with name %s already registered! Failed to register", cmd.GetName()))
      return
    }
  }
  commandList = append(commandList, cmd)
}

func FindCommand(alias string) DiscordCommand{
  if !initialized {
    return nil
  }
  for _,cmd := range commandList {
    for _,a := range cmd.GetAliases() {
      if strings.EqualFold(a, alias){
        return cmd
      }
    }
	}
  return nil
}

func GetCommandList() []DiscordCommand {
  return commandList
}
