package commands

import (
  "github.com/bwmarrin/discordgo";
  "strings";
  "regexp";
)

var commandRegex = regexp.MustCompile("^[\\s]*<@[!]?([0-9]+)>([\\s]+[^\\s]+)[ ]?(.*)")

func CheckCategory(cmd DiscordCommand, channelId string) bool {
  // For Future use to lock down secific Categories in specific Channels
  return true
}

func HandleMessage(message *discordgo.Message, bot *discordgo.Session){
  botUser := bot.State.User
  if message.Author.Bot || len(message.Mentions) < 1 || !isUserMentioned(botUser, message.Mentions) {
    // 1.) Other bots may not execute commands to prevent spam, loops and ddos
    // 2.) In order to trigger a command the bot has to be mentioned, so no mentions exclude further handling
    // 3.) As sayed ... Bot has to be mentioned
    return;
  }

  commandMatch := commandRegex.FindStringSubmatch(message.Content)
  if commandMatch == nil || len(commandMatch) < 3 || botUser.ID != commandMatch[1] {
    return;
  }

  argStr := ""
  if len(commandMatch) > 3 {
    argStr = strings.Trim(commandMatch[3], " ")
  }

  var cmdArgs []string
  if len(argStr) > 0 {
    cmdArgs = strings.Split(argStr, " ")
  } else {
    cmdArgs = []string{}
  }

  cmd := FindCommand(strings.Trim(commandMatch[2], " "))

  if cmd == nil {
    ReactoToCommand(CMDREACT_UNKNOWN, message.ID, message.ChannelID, bot)
  } else if !CheckCategory(cmd, message.ChannelID) {
    ReactoToCommand(CMDREACT_BLOCKED, message.ID, message.ChannelID, bot)
  } else {
    cmd.Execute(commandMatch[2], cmdArgs, message, bot)
  }
}

type CommandReactType int;
const (
  CMDREACT_UNKNOWN CommandReactType = iota;
  CMDREACT_BLOCKED
  CMDREACT_NOPERMISSION
  CMDREACT_EXECERROR
)

func ReactoToCommand(typ CommandReactType, messageID string, channelID string, bot *discordgo.Session) bool {
  reaction := ""

  switch typ {
  case CMDREACT_UNKNOWN:
    reaction = "❓"
    break;
  case CMDREACT_BLOCKED:
    reaction = "🚫"
    break;
  case CMDREACT_NOPERMISSION:
    reaction = "🛂"
    break;
  case CMDREACT_EXECERROR:
    reaction = "⛔"
    break;
  }

  if reaction != "" {
    err := bot.MessageReactionAdd(channelID, messageID, reaction)
    return err != nil
  }
  return false
}

func isUserMentioned(user *discordgo.User, mentions []*discordgo.User) bool {
  for _, mention := range mentions {
    if user.ID == mention.ID {
      return true
    }
  }
  return false
}
