package handle

import (
  "github.com/bwmarrin/discordgo"
  cmdImpl "../impl"
  cmd ".."
  help "../help"
)

func Init() {
  cmd.InitCommands()
  cmd.RegisterCommand(new(cmdImpl.BotInfo))
  cmd.RegisterCommand(new(cmdImpl.SayHello))
  cmd.RegisterCommand(new(cmdImpl.Minime))
  cmd.RegisterCommand(new(cmdImpl.HelpCmd))
  cmd.RegisterCommand(new(cmdImpl.BigEmoji))

  help.InitHelp()
}

func HandleMessage(message *discordgo.Message, bot *discordgo.Session){
  go cmd.HandleMessage(message, bot)
}
