package help

type HelpCategoryEntry struct {
  Emoji string
  CategoryPath []string
}
