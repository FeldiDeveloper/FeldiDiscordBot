package help

import (
    cmd ".."
    "strings"
    logging "github.com/op/go-logging";
    "fmt"
)

var log = logging.MustGetLogger("DiscordHelp")
var emoteList []string
var helpList []HelpCategoryEntry
var initialized = false;

func InitHelp(){
  helpList = make([]HelpCategoryEntry, 0, 8)
  emoteList = make([]string, 0, 8)
  initialized = true;

  RegisterHelp(HelpCategoryEntry{"🎉", []string{"fun"} })
  RegisterHelp(HelpCategoryEntry{"📔", []string{"general"} })
}

func RegisterHelp(helpCat HelpCategoryEntry){
  if !initialized {
    log.Error(fmt.Sprintf("Help list not initialized! Failed to register %s", strings.Join(helpCat.CategoryPath,".")))
    return
  }
  helpList = append(helpList, helpCat)

  known := false
  for _,emote := range emoteList {
    if emote == helpCat.Emoji {
      known = true
      break
    }
  }

  if !known {
    emoteList = append(emoteList, helpCat.Emoji)
  }
}

func BuildHelpPage(page int) (string, int) {
  if page <= 0 || page > len(emoteList) {
    message := "**Hilfe**\n\n"

    for _,emote := range emoteList {
      message += "[ "+emote+" ]\n"

      cats := GetCategories(emote)
      for _, cat := range cats {
        message += ">> " + strings.Join(cat.CategoryPath, ".") + "\n"
      }
      message += "\n"
    }
    message += "🗑 Um diese Nachricht zu löschen"
    return message, 0
  } else {
    emoji := emoteList[page-1]
    message := "**Hilfe für " + emoji + "**\n"
    message2 := ""

    catList := GetCategories(emoji)
    for _, cat := range catList {
      message += "| ``"+ strings.Join(cat.CategoryPath, ".") +"`` "
      message2 += "**" + strings.Join(cat.CategoryPath, ".") + "**\n"
      cmds := GetCommands(cat)
      for _, cmd := range cmds {
        message2 += ">> ``" + cmd.GetName() + "``\n"
        message2 += "    Aliases: "+strings.Join(cmd.GetAliases(), ", ")+"\n"
        message2 += "    Kategorie: "+strings.Join(cmd.GetCategory(), ".")+"\n"
      }
      message2 +="\n"
    }

    message += "\n\n" + message2
    message += "\n🗑 Um diese Nachricht zu löschen"
    message += "\n🏠 Um zurück zu Übersicht zu gelangen"
    return message, page
  }
}

func GetEmotePage(emoteId string) (int, bool){
  for i,emote := range emoteList {
    if emoteId == emote {
      return i + 1, true
    }
  }
  return 0, false
}

func GetCategories(emoteId string) []HelpCategoryEntry {
  list := make([]HelpCategoryEntry, 0, len(helpList))
  for _,cat := range helpList {
    if cat.Emoji == emoteId{
      list = append(list, cat)
    }
  }
  return list
}

func GetEmotes() []string {
  return emoteList
}

func GetCommands(category HelpCategoryEntry) []cmd.DiscordCommand {
  commands := cmd.GetCommandList()
  list := make([]cmd.DiscordCommand, 0, len(commands))
  for _,cmd := range commands {
    if checkCommandCat(category, cmd) {
      list = append(list, cmd)
    }
  }
  return list
}

func checkCommandCat(category HelpCategoryEntry, command cmd.DiscordCommand) bool {
  cmdCat := command.GetCategory()
  for i, catPiece := range category.CategoryPath {
    if i >= len(cmdCat) || strings.EqualFold(catPiece, cmdCat[i]) {
      return true
    }
  }

  return false
}
