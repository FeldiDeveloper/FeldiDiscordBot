package impl

import (
	"regexp"

	"../../lib/utils"
	"github.com/bwmarrin/discordgo"
)

var emoteRegex = regexp.MustCompile("<[^:]*:[^:]+:([0-9]+)>")

type BigEmoji struct {
}

func (*BigEmoji) GetAliases() []string {
	return []string{"emoji", "bigemoji", "bigemote", "emote"}
}

func (*BigEmoji) GetName() string {
	return "bigemoji"
}

func (*BigEmoji) GetCategory() []string {
	return []string{"fun", "memes"}
}

const (
	Emote_Discord int = 0
	Emote_UTF8    int = 1
)

func (*BigEmoji) Execute(alias string, args []string, message *discordgo.Message, bot *discordgo.Session) {
	if len(args) < 1 || len(args) > 2 {
		bot.ChannelMessageSend(message.ChannelID, message.Author.Mention()+"\nBitte gibt ein Emoji an!")
		return
	}

	emoteType := Emote_Discord
	emotePreparsed := ""

	if len(args) == 2 && args[0] == "id" {
		emotePreparsed = args[1]
	} else {
		emoteMatch := emoteRegex.FindStringSubmatch(args[0])
		if emoteMatch != nil || len(emoteMatch) >= 2 {
			emotePreparsed = emoteMatch[1]
		} else {
			emoteType = Emote_UTF8
			emotePreparsed = args[0]
		}

	}

	url := ""

	switch emoteType {
	case Emote_UTF8:
		url = utils.EmoteToTwemoji(emotePreparsed, "72x72", "png")
		status, error := utils.CheckHTTP(url)
		if status != 200 || error != nil {
			bot.ChannelMessageSend(message.ChannelID, message.Author.Mention()+"\nKonnte Emote nicht in der Datenbank (UTF-8 Twitter Emotes) finden!")
			return
		}
		break
	case Emote_Discord:
		url = discordgo.EndpointCDN + "emojis/" + emotePreparsed
		animated := true
		status, error := utils.CheckHTTP(url + ".gif")
		if status != 200 || error != nil {
			animated = false
			status, error = utils.CheckHTTP(url + ".png")
		}

		if status != 200 || error != nil {
			bot.ChannelMessageSend(message.ChannelID, message.Author.Mention()+"\nKonnte Emote nicht auf den Discord servern finden!")
			return
		}

		if animated {
			url += ".gif"
		} else {
			url += ".png"
		}
		break
	default:
		return
	}

	bot.ChannelMessageSend(message.ChannelID, message.Author.Mention()+"\n"+url)
}
