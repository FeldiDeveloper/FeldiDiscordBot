package impl

import (
  "github.com/bwmarrin/discordgo"
  defs "../../definitions"
  "strings"
  "time"
)

type BotInfo struct {

}

func (*BotInfo) GetAliases() []string {
  return []string{"information", "botinfo", "info", "bot"}
}

func (*BotInfo) GetName() string {
  return "info"
}

func (*BotInfo) GetCategory() []string {
  return []string{ "general", "info" }
}

func (*BotInfo) Execute(alias string, args []string, message *discordgo.Message, bot *discordgo.Session) {
  guildNick := ""
  channel, err := bot.Channel(message.ChannelID)
  if err == nil {
    guildMember, err := bot.GuildMember(channel.GuildID, bot.State.User.ID)
    if err == nil {
      guildNick = guildMember.Nick
    }
  }

  if guildNick == "" && channel.Type != discordgo.ChannelTypeDM {
    guildNick = bot.State.User.Username
  }

  embedMessage := &discordgo.MessageEmbed{}
  embedMessage.Author = &discordgo.MessageEmbedAuthor{"", bot.State.User.Username, bot.State.User.AvatarURL("64"), ""}
  embedMessage.Color = 0xafff02
  embedMessage.Title = "Info"
  embedMessage.Timestamp = time.Now().UTC().Format(time.RFC3339)
  embedMessage.Description = "Information for " + defs.BOT_NAME

  fields := make([]*discordgo.MessageEmbedField, 0, 3)
  if guildNick != "" {
    fields = append(fields, &discordgo.MessageEmbedField{"Name on Server", guildNick, true})
  }
  fields = append(fields, &discordgo.MessageEmbedField{"Version", defs.BOT_VERSION, true})
  fields = append(fields, &discordgo.MessageEmbedField{"Developer(s)", strings.Join(defs.BOT_DEVS, ", "), false})

  embedMessage.Fields = fields
  embedMessage.Footer = &discordgo.MessageEmbedFooter{"Written in " + defs.BOT_LANG, "", ""}
  bot.ChannelMessageSendEmbed(message.ChannelID, embedMessage)
}
