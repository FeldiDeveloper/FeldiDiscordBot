package impl

import (
  "github.com/bwmarrin/discordgo"
  //cmd ".."
  "fmt"
  help "../help"
  react "../../reacting"
)

type HelpCmd struct {

}

func (*HelpCmd) GetAliases() []string {
  return []string{"help", "hilfe", "?"}
}

func (*HelpCmd) GetName() string {
  return "help"
}

func (*HelpCmd) GetCategory() []string {
  return []string{ "general", "support" }
}

func (*HelpCmd) Execute(alias string, args []string, message *discordgo.Message, bot *discordgo.Session) {
  pmChannel, err := bot.UserChannelCreate(message.Author.ID)
  if err != nil {
    return
  }
  hmsg, npage := help.BuildHelpPage(0)
  sent, err := bot.ChannelMessageSend(pmChannel.ID, react.TagSetMessage(hmsg, fmt.Sprintf("help_%d",npage)))
  if err != nil {
    return
  }
  react.HandleMessagePost("help", sent, pmChannel, bot)
}
