package impl

import (
  "github.com/bwmarrin/discordgo"
  "fmt"
  "io"
  "../../lib/mojangRest"
  "../../lib/utils"
  "../../lib/minimelib"
  "../../lib/mcskinlib"
  "github.com/google/uuid"
  logging "github.com/op/go-logging"
  png "image/png"
  "time"
  "strconv"
)

type Minime struct {

}

func (*Minime) GetAliases() []string {
  return []string{ "minime" }
}

func (*Minime) GetName() string {
  return "minime"
}

func (*Minime) GetCategory() []string {
  return []string{ "fun", "minecraft" }
}

func (*Minime) Execute(alias string, args []string, message *discordgo.Message, bot *discordgo.Session) {

  if len(args) < 1 && len(message.Attachments) < 1 {
    bot.ChannelMessageSend(message.ChannelID, message.Author.Mention()+"\nBitte gibt einen Minecraft Namen oder eine UUID eines Spielers an,\n"+
      "oder lade ein png/jpg hoch wobei du den Befehl also Kommentar schreibst")
    return;
  }

  if len(args) >= 1 {
    if len(args) >= 2 && args[0] == "#" {
      bot.ChannelTyping(message.ChannelID)
      minime_runUrl(args[1], nil, false, message, bot)
    } else if args[0] == "#" {
      bot.ChannelMessageSend(message.ChannelID, message.Author.Mention()+"\nEs fehlt ein Link zur Textur!")
    } else {
      bot.ChannelTyping(message.ChannelID)
      minime_runArg(args[0], message, bot)
    }
  } else if len(message.Attachments) >= 1 {
    bot.ChannelTyping(message.ChannelID)
    minime_runUrl(message.Attachments[0].URL, nil, false, message, bot)
  }
}

func minime_runUrl(url string, profile *mojangRest.ProfileResponse, alex bool, message *discordgo.Message, bot *discordgo.Session) {
  image, err := utils.DownloadImage(url, 1024 * 1024) //Max 1MB Image

  if err != nil {
    bot.ChannelMessageSend(message.ChannelID, fmt.Sprintf("%s Konnte die Textur nicht herunterladen! Link: %s", message.Author.Mention(), url))
    return
  }

  mcskin, success := mcskinlib.ConvertToSkin(image, alex)
  if !success {
    bot.ChannelMessageSend(message.ChannelID, fmt.Sprintf("%s Die Texturmaße sind nicht korrekt. Bitte überprüfe die Texur nochmal. %s", message.Author.Mention(), url))
    return
  }

  minime := minimelib.MakeMiniMe(mcskin, true, true)

  pipeRd, pipeWr := io.Pipe()

  name := ""
  if profile == nil {
    name = "minime.png"
  } else {
    name = "minime_" + profile.Id + ".png"
  }

  go func() {
    messageSend := &discordgo.MessageSend{}

    embedMessage := &discordgo.MessageEmbed{}
    embedMessage.Author = &discordgo.MessageEmbedAuthor{"", message.Author.Username, message.Author.AvatarURL("64"), ""}
    embedMessage.Title = "MiniMe"
    embedMessage.Color = 0x00ffff
    embedMessage.Timestamp = time.Now().UTC().Format(time.RFC3339)

    fields := make([]*discordgo.MessageEmbedField, 0, 4)
    fields = append(fields, &discordgo.MessageEmbedField{"Legacy Skin", strconv.FormatBool(mcskin.Legacy), true})
    fields = append(fields, &discordgo.MessageEmbedField{"Alex Model", strconv.FormatBool(alex), true})
    if profile != nil && profile.Name != "" && profile.Id != ""{
      fields = append(fields, &discordgo.MessageEmbedField{"Name", profile.Name, false})
      fields = append(fields, &discordgo.MessageEmbedField{"UUID", profile.Id, false})
    }
    embedMessage.Fields = fields

    messageSend.Files = make([]*discordgo.File, 1)
    messageSend.Files[0] = &discordgo.File{Name: name, Reader: pipeRd}
    messageSend.Embed = embedMessage

    defer pipeRd.Close()
    bot.ChannelMessageSendComplex(message.ChannelID, messageSend)
  }()

  png.Encode(pipeWr,minime)
  pipeWr.Close()

}

func minime_runArg(arg string, message *discordgo.Message, bot *discordgo.Session) {
  isUUid := true
  uid, err := uuid.Parse(arg)
  if err != nil {
      uid, err = mojangRest.CorrectUUid(arg)
      if err != nil {
        isUUid = false
      }
  }

  if !isUUid {
    uid, err = mojangRest.GetUUid(arg)
    if err != nil {
        if err.Error() ==  "unknown player" {
          bot.ChannelMessageSend(message.ChannelID, fmt.Sprintf("%s Es wurde kein Spieler mit dem Namen %s gefunden", message.Author.Mention(), arg))
        } else {
          bot.ChannelMessageSend(message.ChannelID, fmt.Sprintf("%s Es ist ein Fehler mit der Mojang API aufgetreten!", message.Author.Mention()))
          logging.MustGetLogger("Command-MiniMe").Warning("Mojang API Error: "+err.Error())
        }
        return
    }
  }

  profile, err := mojangRest.GetProfile(uid)
  if err != nil {
      if err.Error() ==  "unknown player" {
        bot.ChannelMessageSend(message.ChannelID, fmt.Sprintf("%s Es wurde kein Spieler mit der UUID %s gefunden", message.Author.Mention(), uid.String()))
      } else {
        bot.ChannelMessageSend(message.ChannelID, fmt.Sprintf("%s Es ist ein Fehler mit der Mojang API aufgetreten!", message.Author.Mention()))
        logging.MustGetLogger("Command-MiniMe").Warning("Mojang API Error: "+err.Error())
      }
      return
  }

  skinUrl, skinType, err := mojangRest.GetSkin(profile)
  if err != nil {
      bot.ChannelMessageSend(message.ChannelID, fmt.Sprintf("%s Es wurde kein skin für %s gefunden!", message.Author.Mention(), profile.Name))
      return
  }

  minime_runUrl(skinUrl, &profile , skinType=="slim", message, bot)
}
