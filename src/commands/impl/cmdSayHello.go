package impl

import (
  "github.com/bwmarrin/discordgo"
)

type SayHello struct {

}

func (*SayHello) GetAliases() []string {
  return []string{"hello", "hi"}
}

func (*SayHello) GetName() string {
  return "hello"
}

func (*SayHello) GetCategory() []string {
  return []string{ "fun", "memes" }
}

func (*SayHello) Execute(alias string, args []string, message *discordgo.Message, bot *discordgo.Session) {
  bot.ChannelMessageSend(message.ChannelID, "Hello! "+message.Author.Mention())
}
