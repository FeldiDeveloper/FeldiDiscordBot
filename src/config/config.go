package config

import (
	"encoding/xml"
	"bytes"
	"os"
	logging "github.com/op/go-logging"
)

var Log *logging.Logger
var Config *ConfigData

type ConfigData struct {
	XMLName   		xml.Name					`xml:"config"`
	DiscordToken 	string						`xml:"discord>token"`
}

func (c *ConfigData) setDefaults() {
	c.DiscordToken = ""
}

func LoadConfig(path string) bool {
	c := &ConfigData{}
	c.setDefaults()

	file, err := os.OpenFile(path , os.O_RDWR|os.O_CREATE, 0660)
	if err!=nil {
		Log.Error("Error while opening config: " +err.Error())
		return false
	}
	defer file.Close()

  stat, err3 := file.Stat()
	if err3==nil && stat.Size() > 0 {
		buf := &bytes.Buffer{}
		buf.ReadFrom(file)
		err2 := xml.Unmarshal(buf.Bytes(), &c)
		if err2!=nil {
			Log.Error("Error while reading config: " +err2.Error())
			return false;
		}
	}
	file.Seek(0,0)

	enc := xml.NewEncoder(file)
	enc.Indent("", "  ")
	err4 := enc.Encode(c)
	if err4!=nil {
		Log.Warning("Error while writing config: " +err3.Error())
	}

	Config = c
	return true
}
