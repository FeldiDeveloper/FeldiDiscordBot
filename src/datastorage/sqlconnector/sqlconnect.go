package sqlconnector

import (

  logging "github.com/op/go-logging"
)

var log = logging.MustGetLogger("MySQL-Connector")
