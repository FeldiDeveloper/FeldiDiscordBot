package discord

import(
  "github.com/bwmarrin/discordgo"
  "fmt"
  logging "github.com/op/go-logging"
  commands "../commands/handle"
  reacting "../reacting/handle"
  "time"
)

var Log = logging.MustGetLogger("DiscordWrapping")
var logInternal = logging.MustGetLogger("DiscordGo")
var Bot *discordgo.Session

func logReplace(msgL, caller int, format string, a ...interface{}) {
  msg := fmt.Sprintf(format, a...)
  switch msgL{
  case discordgo.LogError:
    logInternal.Error(msg)
    break;
  case discordgo.LogWarning:
    logInternal.Warning(msg)
    break;
  case discordgo.LogInformational:
    logInternal.Info(msg)
    break;
  case discordgo.LogDebug:
    logInternal.Debug(msg)
    break;
  }
}

func StartBot(token string){
  bot, err := discordgo.New("Bot "+token)
  if err != nil {
    Log.Error("Bot failed to create: " + err.Error())
    return
  }

  discordgo.Logger = logReplace

  bot.AddHandler(handle_messageCreate)
  bot.AddHandler(handle_reactionAdded)
  bot.AddHandler(handle_reactionRemoved)
  bot.AddHandler(handle_reactionRemovedAll)
  bot.AddHandler(handle_ready)
  bot.AddHandler(handle_guildCreate)
  bot.AddHandler(handle_guildDelete)

  commands.Init()
  reacting.Init()

  go startBotLoop(bot)
}

func startBotLoop(bot *discordgo.Session){
  ticker := time.NewTicker(30)
  defer ticker.Stop()
  var err error
  for Bot == nil {
    err = bot.Open()
    if err == nil {
      Bot = bot
      return
    } else {
      Log.Error("Bot failed to connect: " + err.Error())
      <-ticker.C
    }
  }
}

func StopBot(){
  if Bot!=nil{
    Bot.Close()
  }
}
