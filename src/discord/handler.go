package discord

import(
  "github.com/bwmarrin/discordgo"
  commands "../commands/handle"
  reacting "../reacting/handle"
  "fmt"
)


func handle_messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}
  commands.HandleMessage(m.Message, s)
}

func handle_reactionAdded(s *discordgo.Session, m *discordgo.MessageReactionAdd) {
	if m.UserID == s.State.User.ID {
		return
	}
  reacting.HandleReactionAdded(m.MessageReaction, s)
}

func handle_reactionRemoved(s *discordgo.Session, m *discordgo.MessageReactionRemove) {
	if m.UserID == s.State.User.ID {
		return
	}
  reacting.HandleReactionRemoved(m.MessageReaction, s)
}

func handle_reactionRemovedAll(s *discordgo.Session, m *discordgo.MessageReactionRemoveAll) {
	if m.UserID == s.State.User.ID {
		return
	}
  reacting.HandleReactionRemoveAll(m.MessageReaction, s)
}

func handle_ready(s *discordgo.Session, event *discordgo.Ready){
  Log.Info(fmt.Sprintf("Shard %d is ready (Total %d)", s.ShardID, s.ShardCount))
}

func handle_guildCreate(s *discordgo.Session, event *discordgo.GuildCreate){
  Log.Info(fmt.Sprintf("Joined Guild: %s(%s)", event.Name, event.ID))
}

func handle_guildDelete(s *discordgo.Session, event *discordgo.GuildDelete){
  Log.Info(fmt.Sprintf("Leave Guild: %s(%s)", event.Name, event.ID))
}
