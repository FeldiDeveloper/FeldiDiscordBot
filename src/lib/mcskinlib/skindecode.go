package mcskinlib

import (
  "image";
  imaging "github.com/disintegration/imaging";
)

type Side int
const (
  Side_TOP Side = iota
  Side_BOTTOM
  Side_LEFT
  Side_RIGHT
  Side_FRONT
  Side_BACK
)

func GetSides() []Side {
  return []Side{
    Side_TOP,
    Side_BOTTOM,
    Side_LEFT,
    Side_RIGHT,
    Side_FRONT,
    Side_BACK,
  }
}

type CubeType int
const (
  Cube_HEAD CubeType = iota
  Cube_HAT
  Cube_BODY
  Cube_LEG_LEFT
  Cube_LEG_RIGHT
  Cube_ARM_LEFT
  Cube_ARM_RIGHT
  Cube_BODY_OVER
  Cube_LEG_LEFT_OVER
  Cube_LEG_RIGHT_OVER
  Cube_ARM_LEFT_OVER
  Cube_ARM_RIGHT_OVER
)

func GetCubeTypes() []CubeType {
  return []CubeType {
    Cube_HEAD,
    Cube_HAT,
    Cube_BODY,
    Cube_LEG_LEFT,
    Cube_LEG_RIGHT,
    Cube_ARM_LEFT,
    Cube_ARM_RIGHT,
    Cube_BODY_OVER,
    Cube_LEG_LEFT_OVER,
    Cube_LEG_RIGHT_OVER,
    Cube_ARM_LEFT_OVER,
    Cube_ARM_RIGHT_OVER,
  }
}

type TextureCube struct{
  width uint; // Front face left to right
  height uint; // Front face top to bottom
  length uint; // Right face left to right
  bottomLeft image.Point; // Bottom-left startpoint of the cube texture
  mirror bool; // If the images should get mirrored. Required for legacy models
}

type MinecraftSkin struct{
  Alex bool;
  Legacy bool;
  Texture [][]image.Image;
}

func GenerateCubes(alex bool, legacy bool) []TextureCube {
  var cubes []TextureCube = make([]TextureCube, 12);

  cubes[Cube_HEAD] = TextureCube{8,8,8, image.Point{0, 16}, false}
  cubes[Cube_HAT] = TextureCube{8,8,8, image.Point{32, 16}, false}

  cubes[Cube_BODY] = TextureCube{8,12,4, image.Point{16, 32}, false}
  cubes[Cube_LEG_RIGHT] = TextureCube{4,12,4, image.Point{0, 32}, false}
  if !legacy {
    cubes[Cube_BODY_OVER] = TextureCube{8,12,4, image.Point{16, 48}, false}
    cubes[Cube_LEG_RIGHT_OVER] = TextureCube{4,12,4, image.Point{0, 48}, false}
    cubes[Cube_LEG_LEFT] = TextureCube{4,12,4, image.Point{16, 64}, false}
    cubes[Cube_LEG_LEFT_OVER] = TextureCube{4,12,4, image.Point{0, 64}, false}
  }

  if alex {
    cubes[Cube_ARM_RIGHT] = TextureCube{3,12,4, image.Point{40, 32}, false}
    if !legacy {
      cubes[Cube_ARM_RIGHT_OVER] = TextureCube{3,12,4, image.Point{40, 48}, false}
      cubes[Cube_ARM_LEFT] = TextureCube{3,12,4, image.Point{32, 64}, false}
      cubes[Cube_ARM_LEFT_OVER] = TextureCube{3,12,4, image.Point{48, 64}, false}
    }
  } else {
    cubes[Cube_ARM_RIGHT] = TextureCube{4,12,4, image.Point{40, 32}, false}
    if !legacy {
      cubes[Cube_ARM_RIGHT_OVER] = TextureCube{4,12,4, image.Point{40, 48}, false}
      cubes[Cube_ARM_LEFT] = TextureCube{4,12,4, image.Point{32, 64}, false}
      cubes[Cube_ARM_LEFT_OVER] = TextureCube{4,12,4, image.Point{48, 64}, false}
    }
  }

  if legacy {
    cubes[Cube_ARM_LEFT] = cubes[Cube_ARM_RIGHT]
    cubes[Cube_ARM_LEFT].mirror = true

    cubes[Cube_LEG_LEFT] = cubes[Cube_LEG_RIGHT]
    cubes[Cube_LEG_LEFT].mirror = true
  }

  return cubes
}

func CalculateRectangle(cube TextureCube, side Side) image.Rectangle{
  var sizeX, sizeY uint
  var offX, offY int

  switch side {
  case Side_TOP:
    offX = int(cube.length)
    offY = -int(cube.height)
    sizeX = cube.width
    sizeY = cube.length
    break;
  case Side_BOTTOM:
    offX = int(cube.length + cube.width)
    offY = -int(cube.height)
    sizeX = cube.width
    sizeY = cube.length
    break;
  case Side_LEFT:
    offX = int(cube.length + cube.width)
    offY = 0
    sizeX = cube.length
    sizeY = cube.height
    break;
  case Side_RIGHT:
    offX = 0
    offY = 0
    sizeX = cube.length
    sizeY = cube.height
    break;
  case Side_FRONT:
    offX = int(cube.length)
    offY = 0
    sizeX = cube.width
    sizeY = cube.height
    break;
  case Side_BACK:
    offX = int(( cube.length * 2 ) + cube.width)
    offY = 0
    sizeX = cube.width
    sizeY = cube.height
    break;
  }

  min := image.Point{ cube.bottomLeft.X + offX , cube.bottomLeft.Y + offY - int(sizeY)}
  max := image.Point{ min.X + int(sizeX) , min.Y + int(sizeY) }
  return image.Rectangle{min, max}
}

func IsLegacy(input image.Image) bool{
  bonds := input.Bounds()
  height := bonds.Max.Y - bonds.Min.Y
  return height == 32
}

func ValidImage(input image.Image) bool{
  bonds := input.Bounds()
  height := bonds.Max.Y - bonds.Min.Y
  width := bonds.Max.X - bonds.Min.X
  return (height == 32 || height == 64) && width == 64
}

func CutImage(input image.Image, cubes []TextureCube) [][]image.Image {
  var imagemap [][]image.Image = make([][]image.Image, len(cubes))
  sides := GetSides()
  sideLen := len(sides)
  for cube, element := range cubes {
    imagemap[cube] = make([]image.Image, sideLen)
    for _, side := range sides {
      image := imaging.Crop(input, CalculateRectangle(element, side))
      if element.mirror {
        image = imaging.FlipH(image)
      }
      imagemap[cube][side] = image
    }
  }
  return imagemap
}

func ConvertToSkin(input image.Image, alex bool) (MinecraftSkin, bool) {
  if !ValidImage(input){
    return MinecraftSkin{}, false
  }

  skin := MinecraftSkin{}
  skin.Legacy = IsLegacy(input)
  skin.Alex = alex
  skin.Texture = CutImage(input, GenerateCubes(skin.Alex, skin.Legacy))
  return skin, true
}
