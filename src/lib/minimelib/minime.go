package minimelib

import (
  "../mcskinlib"
  "image";
  "image/color";
  imaging "github.com/disintegration/imaging";
)

func MakeMiniMe(skin mcskinlib.MinecraftSkin, useOverlays bool, useHat bool) image.Image{
  return MakeMiniMeWithResample(skin, useOverlays, useHat, imaging.NearestNeighbor)
}

func MakeMiniMeWithResample(skin mcskinlib.MinecraftSkin, useOverlays bool, useHat bool, filter imaging.ResampleFilter) image.Image{
  useOverlays = useOverlays && !skin.Legacy

  headTex := imaging.Clone(skin.Texture[mcskinlib.Cube_HEAD][mcskinlib.Side_FRONT]);
  if useHat {
    headTex = imaging.Overlay(headTex, skin.Texture[mcskinlib.Cube_HAT][mcskinlib.Side_FRONT], image.Point{0,0}, 1.0)
  }

  legLTex := imaging.Clone(skin.Texture[mcskinlib.Cube_LEG_LEFT][mcskinlib.Side_FRONT]);
  legRTex := imaging.Clone(skin.Texture[mcskinlib.Cube_LEG_RIGHT][mcskinlib.Side_FRONT]);
  bodyTex := imaging.Clone(skin.Texture[mcskinlib.Cube_BODY][mcskinlib.Side_FRONT]);
  armLTex := imaging.Clone(skin.Texture[mcskinlib.Cube_ARM_LEFT][mcskinlib.Side_FRONT]);
  armRTex := imaging.Clone(skin.Texture[mcskinlib.Cube_ARM_RIGHT][mcskinlib.Side_FRONT]);

  if(useOverlays){
    legLTex = imaging.Overlay(legLTex, skin.Texture[mcskinlib.Cube_LEG_LEFT_OVER][mcskinlib.Side_FRONT], image.Point{0,0}, 1.0)
    legRTex = imaging.Overlay(legRTex, skin.Texture[mcskinlib.Cube_LEG_RIGHT_OVER][mcskinlib.Side_FRONT], image.Point{0,0}, 1.0)
    bodyTex = imaging.Overlay(bodyTex, skin.Texture[mcskinlib.Cube_BODY_OVER][mcskinlib.Side_FRONT], image.Point{0,0}, 1.0)
    armLTex = imaging.Overlay(armLTex, skin.Texture[mcskinlib.Cube_ARM_LEFT_OVER][mcskinlib.Side_FRONT], image.Point{0,0}, 1.0)
    armRTex = imaging.Overlay(armRTex, skin.Texture[mcskinlib.Cube_ARM_RIGHT_OVER][mcskinlib.Side_FRONT], image.Point{0,0}, 1.0)
  }

  legLTex = imaging.Resize(legLTex, 1, 3, filter)
  legRTex = imaging.Resize(legRTex, 1, 3, filter)
  bodyTex = imaging.Resize(bodyTex, 2, 3, filter)
  armLTex = imaging.Resize(armLTex, 1, 3, filter)
  armRTex = imaging.Resize(armRTex, 1, 3, filter)

  minimeImg := imaging.New(10,17, color.Transparent)

  for x := 0; x < 10; x++ {
    for y := 0; y < 10; y++ {
      minimeImg.Set(x, y, color.Black)
    }
  }

  for x := 1; x < 9; x++ {
    for y := 10; y < 14; y++ {
      minimeImg.Set(x, y, color.Black)
    }
  }

  for x := 3; x < 7; x++ {
    for y := 14; y < 17; y++ {
      minimeImg.Set(x, y, color.Black)
    }
  }


  minimeImg = imaging.Overlay(minimeImg, headTex, image.Point{1,1}, 1.0)
  minimeImg = imaging.Overlay(minimeImg, armRTex, image.Point{2,10}, 1.0)
  minimeImg = imaging.Overlay(minimeImg, armLTex, image.Point{7,10}, 1.0)
  minimeImg = imaging.Overlay(minimeImg, bodyTex, image.Point{4,10}, 1.0)
  minimeImg = imaging.Overlay(minimeImg, legRTex, image.Point{4,13}, 1.0)
  minimeImg = imaging.Overlay(minimeImg, legLTex, image.Point{5,13}, 1.0)

  finalOut := imaging.New(128,128, color.Transparent)
  minimeImg = imaging.Resize(minimeImg, 0, 128, imaging.NearestNeighbor)
  finalOut = imaging.OverlayCenter(finalOut, minimeImg, 1.0)
  return finalOut
}
