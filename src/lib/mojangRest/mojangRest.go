package mojangRest

import (
  "net/http"
  "net/url"
  "encoding/json"
  "github.com/google/uuid"
  "fmt"
  "errors"
  "strings"
  "encoding/base64"
)

func request(url string, obj interface{}) (error, int){
  req, err := http.NewRequest("GET", url, nil)
  if err != nil {
		return err,0
	}
  client := &http.Client{}
  header := http.Header{}

  header.Add("User-Agent", "Mozilla/5.0")
  req.Header = header

  resp, err := client.Do(req)
	if err != nil {
		return err,0
	}
  defer resp.Body.Close()

  err = json.NewDecoder(resp.Body).Decode(obj)
  if err != nil {
		return err, resp.StatusCode
	}

  return nil, resp.StatusCode
}

func CorrectUUid(uid string) (uuid.UUID, error) {
  if len(uid) != 32 {
    return uuid.UUID{}, errors.New(fmt.Sprintf("Wrong UUID legth: %d", len(uid)))
  }

  correctUUid := uid
  correctUUid = correctUUid[:8] + "-" + correctUUid[8:12] + "-" + correctUUid[12:16] + "-" + correctUUid[16:20] + "-" + correctUUid[20:]

  return uuid.Parse(correctUUid)
}

func GetUUid(username string) (uuid.UUID, error) {
  var uuidResp UuidResponse
  err, status := request("http://api.mojang.com/users/profiles/minecraft/"+url.QueryEscape(username), &uuidResp)
  if err != nil {
    if status == 204 {
      return uuid.UUID{}, errors.New("unknown player")
    }
    return uuid.UUID{}, err
  }

  return CorrectUUid(uuidResp.Id)
}

func GetProfile(uid uuid.UUID) (ProfileResponse, error) {
  var profileResp ProfileResponse
  uuidStr := strings.Replace(uid.String(), "-", "", -1)
  err, status := request("http://sessionserver.mojang.com/session/minecraft/profile/"+url.QueryEscape(uuidStr), &profileResp)
  if err != nil {
    if status == 204 {
      return ProfileResponse{}, errors.New("unknown player")
    }
    return ProfileResponse{}, err
  }
  return profileResp, nil
}

func GetSkin(profile ProfileResponse) (string, string, error) {
  texProp := profile.SearchPropertyWithName("textures")
  if texProp == nil {
    return "", "", errors.New("no texture property")
  }

  textureValue, err := base64.StdEncoding.DecodeString(texProp.Value)
  if err != nil {
    return "", "", err
  }

  var textureJson map[string]interface{}
  err = json.NewDecoder(strings.NewReader(string(textureValue))).Decode(&textureJson)
  if err != nil {
		return "", "", err
	}
  texture2Value,ok := textureJson["textures"].(map[string]interface{})
  if !ok {
		return "", "", errors.New("no skin")
	}
  skinVal,ok := texture2Value["SKIN"]
  if !ok {
		return "", "", errors.New("no skin")
	}
  skinValSafe,ok := skinVal.(map[string]interface{})
  if !ok {
		return "", "", errors.New("no skin")
	}
  url, ok := skinValSafe["url"]
  if !ok {
		return "", "", errors.New("no skin")
	}
  urlSafe, ok := url.(string)
  if !ok {
    return "", "", errors.New("no skin")
  }

  var model = "normal"
  metadata, ok := skinValSafe["metadata"]
  if ok {
    metadataSave, ok := metadata.(map[string]interface{})
    if ok {
      modelUnsafe, ok := metadataSave["model"]
      if ok {
        modelSafe, ok := modelUnsafe.(string)
        if ok {
          model = modelSafe
        }
      }
    }
  }

  return urlSafe, model, nil
}
