package mojangRest

type UuidResponse struct {
  Id string `json:"id"`
  Name string `json:"name"`
  Legacy bool `json:"legacy,omitempty"`
  Demo bool `json:"demo,omitempty"`
}

type ProfileProperties struct {
  Name string `json:"name"`
  Value string `json:"value"`
}

type ProfileResponse struct {
  Id string `json:"id"`
  Name string `json:"name"`
  Properties []ProfileProperties `json:"properties"`
}

func (this *ProfileResponse) SearchPropertyWithName(name string) *ProfileProperties {
  for i, prop := range this.Properties {
    if prop.Name == name {
      return &this.Properties[i]
    }
  }
  return nil
}
