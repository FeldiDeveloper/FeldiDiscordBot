package utils

import (
  "net/http"
  "io"
  "image"
  _ "image/jpeg"
  _ "image/png"
)

func DownloadImage(url string, maxSize int64) (image.Image, error) {
  req, err := http.NewRequest("GET", url, nil)
  if err != nil {
		return nil, err
	}
  client := &http.Client{}
  header := http.Header{}

  header.Add("User-Agent", "Mozilla/5.0")
  req.Header = header

  resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
  body := io.LimitReader(resp.Body, maxSize)
  defer resp.Body.Close()

  img, _, err := image.Decode(body)
  return img, err
}

func CheckHTTP(url string) (int, error) {
  req, err := http.NewRequest("GET", url, nil)
  if err != nil {
    return -1, err
  }
  client := &http.Client{}
  header := http.Header{}

  header.Add("User-Agent", "Mozilla/5.0")
  req.Header = header

  resp, err := client.Do(req)
  if err != nil {
    return -1, err
  }

  return resp.StatusCode, nil
}
