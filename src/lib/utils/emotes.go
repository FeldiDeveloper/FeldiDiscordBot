package utils

import (
	"fmt"
	"unicode/utf8"
)


func EmoteToTwemoji(emoteStr string, size string, format string) string {
  output := ""
  first := true
  for len(emoteStr) > 0 {
		r, size := utf8.DecodeRuneInString(emoteStr)
    if first {
      output += fmt.Sprintf("%x", r)
      first = false
    } else {
      output += fmt.Sprintf("-%x", r)
    }
		emoteStr = emoteStr[size:]
  }

  if len(output) == 0 {
    return ""
  }

  return fmt.Sprintf("https://twemoji.maxcdn.com/2/%s/%s.%s",size, output, format)
}

