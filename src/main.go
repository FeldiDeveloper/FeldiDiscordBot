package main

import (
  "os"
  "os/signal"
  "syscall"
  "strings"
  "fmt"
  "./config"
  "./discord"
  logging "github.com/op/go-logging"
  defines "./definitions"
)

const ProgName = "DiscordBot"
var Log = logging.MustGetLogger("DiscorBot-Main")


var format = logging.MustStringFormatter(`%{color}%{time:2006/01/02 15:04:05} %{module} [%{level:.4s}]%{color:reset}: %{message}`)

func main() {

	logBackend := logging.NewLogBackend(os.Stdout, "", 0)
	logLeveled := logging.SetBackend(logBackend)
	logLeveled.SetLevel(logging.INFO, "")
	logging.SetFormatter(format)
	logging.SetBackend(logLeveled)

	sigs := make(chan os.Signal, 1)
	end := make(chan bool, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
        	sig := <-sigs
        	fmt.Println("")
        	Log.Notice("Signal: " + strings.ToUpper(sig.String()))
        	end <- true
    	}()

	Log.Info("Starting " + ProgName + " ...")

	if !config.LoadConfig("conf.xml") {
		Log.Warning("Failed to start: Config not ready")
		return;
	}
  Log.Info(fmt.Sprintf("Name: %s / Version: %s / Devs: [%s]", defines.BOT_NAME, defines.BOT_VERSION, strings.Join(defines.BOT_DEVS, ", ")))

  discord.StartBot(config.Config.DiscordToken)
  defer discord.StopBot()

	Log.Info("Started " + ProgName)

	for !<- end {
	}

	defer Log.Info("Stopped " + ProgName)
}
