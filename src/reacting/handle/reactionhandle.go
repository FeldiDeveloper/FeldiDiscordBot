package reacting

import (
  "github.com/bwmarrin/discordgo"
  reactingImpl "../impl"
  reacting ".."
)

func Init() {
  reacting.InitHandles()
  reacting.RegisterHandle(new(reactingImpl.HelpMessage))
  //reacting.RegisterHandle(new(cmdImpl.Minime))
}

func HandleReactionAdded(message *discordgo.MessageReaction, bot *discordgo.Session){
  go reacting.HandleReactionChange(message, true, bot)
}

func HandleReactionRemoved(message *discordgo.MessageReaction, bot *discordgo.Session){
  go reacting.HandleReactionChange(message, false, bot)
}

func HandleReactionRemoveAll(message *discordgo.MessageReaction, bot *discordgo.Session){
  go reacting.HandleReactionChange(message, false, bot)
}
