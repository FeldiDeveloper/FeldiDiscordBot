package reacting

import (
  "github.com/bwmarrin/discordgo"
  reacting ".."
  help "../../commands/help"
  "strings"
  "fmt"
  "regexp"
  "strconv"
)

var handleHelp_regex = regexp.MustCompile(`(?m)help(_([0-9]))?`)

type HelpMessage struct {

}

func (*HelpMessage) GetName() string {
  return "help"
}

func (*HelpMessage) CanHandle(message *discordgo.Message, channel *discordgo.Channel, user *discordgo.User, botState *discordgo.State) bool {
  return channel.Type == discordgo.ChannelTypeDM && strings.HasPrefix(reacting.TagCheckMessage(message.Content), "help") && message.Author.ID == botState.User.ID
}

func (this *HelpMessage) HandleReactionChange(reaction *discordgo.MessageReaction, message *discordgo.Message, channel *discordgo.Channel, user *discordgo.User, added bool, bot *discordgo.Session) {
  if reaction.Emoji.Name == "🗑" {
    bot.ChannelMessageDelete(channel.ID, message.ID)
    return
  }

  match := handleHelp_regex.FindStringSubmatch(reacting.TagCheckMessage(message.Content))
  if match == nil {
    return
  }
  page := 0
  if len(match) >= 3 {
    var err error
    page, err = strconv.Atoi(match[2])
    if err != nil {
      page = 0
    }
  }

  npage := -1
  hmsg := ""

  if page != 0 && reaction.Emoji.Name == "🏠" {
    hmsg, npage = help.BuildHelpPage(0)
  } else if page == 0 {
    var ok bool
    npage, ok = help.GetEmotePage(reaction.Emoji.Name)
    if !ok {
      return
    }
    hmsg, npage = help.BuildHelpPage(npage)
  }

  if npage >= 0 && hmsg != "" {
    hmsg = reacting.TagSetMessage(hmsg, fmt.Sprintf("help_%d",npage))
    bot.ChannelMessageDelete(channel.ID, message.ID)
    msg, err := bot.ChannelMessageSend(channel.ID, hmsg)
    if err == nil {
      this.HandleReactionPostSend(msg, channel, bot)
    }
  }
}

func (*HelpMessage) HandleReactionPostSend(message *discordgo.Message, channel *discordgo.Channel, bot *discordgo.Session){
  match := handleHelp_regex.FindStringSubmatch(reacting.TagCheckMessage(message.Content))
  if match == nil {
    return
  }
  page := 0
  if len(match) >= 3 {
    var err error
    page, err = strconv.Atoi(match[2])
    if err != nil {
      page = 0
    }
  }

  bot.MessageReactionAdd(channel.ID, message.ID, "🗑")
  if page != 0 {
    bot.MessageReactionAdd(channel.ID, message.ID, "🏠")
  } else {
    emotes := help.GetEmotes()
    for _, emote := range emotes {
      bot.MessageReactionAdd(channel.ID, message.ID, emote)
    }
  }

}
