package reacting

import (
  "github.com/bwmarrin/discordgo"
)

type DiscordReactionHandle interface {
    GetName() string;
    CanHandle(message *discordgo.Message, channel *discordgo.Channel, user *discordgo.User, botState *discordgo.State) bool;

    HandleReactionChange(reaction *discordgo.MessageReaction, message *discordgo.Message, channel *discordgo.Channel, user *discordgo.User, added bool, bot *discordgo.Session);
    HandleReactionPostSend(message *discordgo.Message, channel *discordgo.Channel, bot *discordgo.Session)
}
