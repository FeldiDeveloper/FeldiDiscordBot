package reacting

import (
  "github.com/bwmarrin/discordgo"
  logging "github.com/op/go-logging";
  "fmt"
)

var Log = logging.MustGetLogger("DiscordReactions")
var handleList []DiscordReactionHandle
var initialized = false;

func InitHandles() {
  handleList = make([]DiscordReactionHandle, 0, 16)
  initialized = true;
}

func RegisterHandle(handle DiscordReactionHandle){
  if !initialized {
    Log.Error(fmt.Sprintf("Reaction handle list not initialized! Failed to register %s", handle.GetName()))
    return
  }
  for _,handle1 := range handleList {
    if handle1.GetName() == handle.GetName() {
      Log.Error(fmt.Sprintf("Reaction handle %s already registered! Failed to register", handle.GetName()))
      return
    }
  }
  handleList = append(handleList, handle)
}

func FindHandles(message *discordgo.Message, channel *discordgo.Channel, user *discordgo.User, bot *discordgo.Session) []DiscordReactionHandle {
  if !initialized {
    return nil
  }
  handles := make([]DiscordReactionHandle, 0, len(handleList))
  for _,handle := range handleList {
    if handle.CanHandle(message, channel, user, bot.State) {
      handles = append(handles, handle)
    }
	}
  return handles
}

func HandleReactionChange(reaction *discordgo.MessageReaction, added bool, bot *discordgo.Session){
  user, err := bot.User(reaction.UserID)
  if err != nil || user.Bot {
    return
  }

  channel, err := bot.Channel(reaction.ChannelID)
  if err != nil {
    return
  }
  message, err := bot.ChannelMessage(reaction.ChannelID, reaction.MessageID)
  if err != nil {
    return
  }

  handles := FindHandles(message, channel, user, bot)
  for _,handle := range(handles) {
    handle.HandleReactionChange(reaction, message, channel, user, added, bot)
  }
}

func HandleMessagePostAuto(message *discordgo.Message, channel *discordgo.Channel, bot *discordgo.Session) {
  handles := FindHandles(message, channel, bot.State.User, bot)
  for _,handle := range(handles) {
    handle.HandleReactionPostSend(message, channel, bot)
  }
}

func HandleMessagePost(handleId string, message *discordgo.Message, channel *discordgo.Channel, bot *discordgo.Session) bool {
  var handle DiscordReactionHandle = nil
  for _,handle1 := range(handleList) {
    if handle1.GetName() == handleId {
      handle = handle1
    }
  }

  if handle == nil {
    return false
  }

  handle.HandleReactionPostSend(message, channel, bot)
  return true
}
