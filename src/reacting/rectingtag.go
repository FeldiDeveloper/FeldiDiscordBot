package reacting


import (
  "fmt"
  "regexp"
)

var tagRegex = regexp.MustCompile(`(?m).*\n\x60\x60(\w+)\x60\x60`)

func TagCheckMessage(message string) string {
  tagMatch := tagRegex.FindStringSubmatch(message)

  if tagMatch == nil || len(tagMatch) < 2 {
    return ""
  }
  return tagMatch[1]
}

func TagSetMessage(message string, tag string) string {
  return fmt.Sprintf("%s\n``%s``", message, tag)
}
